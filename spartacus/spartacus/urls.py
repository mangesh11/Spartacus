"""spartacus URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from text_search import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.index, name='index'),
    url(r'^registration/$', views.user_registration, name='user_registration'),
    url(r'^new_book/$', views.new_book, name='new_book'),
    url(r'^book-list/', views.book_list, name='book_list'),
    url(r'^text-search/', include('text_search.urls', namespace='text-search')),
    url(r'^pdf-to-text/', views.pdf_to_text_view, name='pdf_to_text'),
    url(r'^download-book/', views.download_book, name='download_book'),
    url(r'^login/', views.user_login, name='user_login'),
    url(r'^logout/', views.user_logout, name='user_logout'),
]

admin.site.site_header = 'SPARTACUS ADMIN'
