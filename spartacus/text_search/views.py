# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect, reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from models import *
from pdf_to_text import convert
from django.conf import settings
import os
import PyPDF2
import re
import json

# Create your views here.


def user_login(request):
    # If the request is a HTTP POST, try to pull out the relevant information.
    if request.method == 'POST':
        # Authenticate
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)

        # If valid login
        if user:
            # Is the account active? It could have been disabled.
            if user.is_active:
                # If the account is valid and active, we can log the user in.
                # We'll send the user back to the homepage.
                login(request, user)
                return HttpResponseRedirect('/')
            else:
                # An inactive account was used - no logging in!
                return HttpResponse("Your account is disabled.")
        else:
            # Bad login details were provided. So we can't log the user in.
            # print "Invalid login details: {0}, {1}".format(username, password)
            return HttpResponse("Invalid login details supplied.")

    else:
        return render(request, 'user/login.html', {})


@login_required(login_url='/login/')
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/login/')


def user_registration(request):
    context_data = dict()
    error = False
    msg = ''
    if request.method == "POST":
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        email = request.POST.get('email')
        username = request.POST.get('username')
        password = request.POST.get('password')
        confirm_password = request.POST.get('confirm_password')

        if not first_name:
            error = True
            msg = 'First Name is required'
        else:
            context_data['first_name'] = first_name

        if not error and not last_name:
            error = True
            msg = 'Last Name is required'
        else:
            context_data['last_name'] = last_name

        if not error and not email:
            error = True
            msg = 'Email is required'
        else:
            context_data['email'] = email

        if not error and not username:
            error = True
            msg = 'Username is required'
        else:
            context_data['username'] = username

        if not error and not password:
            error = True
            msg = 'Password is required'
        else:
            context_data['password'] = password

        if not error and not confirm_password:
            error = True
            msg = 'Confirm Password is required'
        else:
            context_data['confirm_password'] = confirm_password

        if not error and password and confirm_password:
            if password != confirm_password:
                error = True
                msg = 'Password mismatched'

        if not error:
            user = User.objects.create_user(
                username=username,
                first_name=first_name,
                last_name=last_name,
                email=email,
                password=password
            )

            return HttpResponseRedirect('/login/')

    context_data['error'] = error
    context_data['msg'] = msg
    return render(request, 'user/registration.html', context_data)


@login_required(login_url='/login/')
def index(request):
    context_data = dict()
    # search_input = request.GET.get('q', '')
    # if search_input:
    #     context_data['query'] = search_input
    #     import nltk
    #     from nltk.corpus import wordnet
    #
    #     synonyms = []
    #     antonyms = []
    #
    #     for syn in wordnet.synsets(search_input):
    #         for l in syn.lemmas():
    #             synonyms.append(l.name())
    #             if l.antonyms():
    #                 antonyms.append(l.antonyms()[0].name())

        # context_data['synonyms'] = set(synonyms)
        # context_data['antonyms'] = set(antonyms)
    context_data['book_list'] = json.dumps(list(Book.objects.order_by('-id').values('id', 'category_id', 'title')))
    return render(request, 'index.html', context_data)


def pdf_to_text_view(request):
    context_data = dict()
    search_keywords = []
    search_input = request.GET.get('q', '')
    is_synonyms = request.GET.get('is_synonyms', 0)
    is_antonyms = request.GET.get('is_antonyms', 0)
    category_id = request.GET.get('category_id', None)
    book_id = request.GET.get('book_id', None)

    if search_input:
        context_data['query'] = search_input
        search_keywords.append(search_input)
        import nltk
        from nltk.corpus import wordnet

        synonyms = []
        antonyms = []

        for syn in wordnet.synsets(search_input):
            for l in syn.lemmas():
                synonyms.append(l.name())
                if l.antonyms():
                    antonyms.append(l.antonyms()[0].name())

        if int(is_synonyms):
            search_keywords.extend(set(synonyms))
            context_data['total_synonyms'] = ', '.join(set(synonyms))
        if int(is_antonyms):
            search_keywords.extend(set(antonyms))
            context_data['total_antonyms'] = ', '.join(set(antonyms))
        search_keywords = set(search_keywords)
    else:
        return HttpResponse('keywords not found...')

    if category_id and book_id:
        book_obj = Book.objects.filter(category_id=category_id, id=book_id).first()
        file_path = os.path.join(settings.BASE_DIR, 'uploads', str(book_obj.id), book_obj.file_name)
        if os.path.isfile(file_path):
            # search_keywords = ['income', 'of']
            replaced_word = "<strong><span style='color:red'>%s</span></strong>"
            if search_keywords:
                with open(file_path, mode='rb') as f:
                    # creating a pdf reader object
                    pdfReader = PyPDF2.PdfFileReader(f)

                    # printing number of pages in pdf file
                    context_data['pages'] = pdfReader.numPages
                    result_list = []
                    # creating a page object
                    page_count = 1
                    for num in range(pdfReader.numPages):
                        pageObj = pdfReader.getPage(num)
                        # extracting text from page
                        temp_text = pageObj.extractText()
                        result_dict = dict()
                        result_dict['is_present'] = False
                        result_dict['page'] = ''
                        result_dict['data'] =  ''
                        result_dict['search_keyword'] = ''
                        c = 1
                        for keyword in search_keywords:
                            is_present = bool(re.search(r'\b%s\b' % keyword, temp_text))
                            if is_present:
                                temp_text = re.sub(r'\b%s\b' % keyword, replaced_word % keyword, temp_text)
                                if c > 1:
                                    result_dict['search_keyword'] += ', ' + keyword
                                else:
                                    result_dict['search_keyword'] += keyword
                                c += 1
                                result_dict['is_present'] = True
                        if result_dict['is_present']:
                            result_dict['page'] = 'Page ' + str(page_count)
                            result_dict['data'] = temp_text
                            result_list.append(result_dict)
                        page_count += 1
                    context_data['result'] = result_list
            else:
                return HttpResponse('keywords not found...')
        else:
            return HttpResponse('File not found...')
    else:
        return HttpResponse('Category or Book not found...')
    return render(request, 'pdf_to_text.html', context_data)


@login_required(login_url='/login/')
def new_book(request):
    context_data = dict()
    error = False
    msg = ''
    if request.method == "POST":
        category = request.POST.get('category')
        document_title = request.POST.get('document_title')
        document_auther = request.POST.get('document_auther')
        document_file = request.FILES.get('document_file')

        if not category:
            error = True
            msg = 'Category is required'
        else:
            context_data['doc_category'] = int(category)

        if not error and not document_title:
            error = True
            msg = 'Document Title is required'
        else:
            context_data['document_title'] = document_title

        if not error and not document_file:
            error = True
            msg = 'Document File is required'
        else:
            if not str(document_file.name).endswith('.pdf'):
                error = True
                msg = 'Please upload pdf document'

        context_data['document_auther'] = document_auther

        if not error:
            book = Book.objects.create(
                category_id=category,
                title=document_title,
                auther=document_auther
            )

            my_path = os.path.join(settings.BASE_DIR, 'uploads', str(book.id))
            if not os.path.exists(my_path):
                os.makedirs(my_path)
            file_path = os.path.join(my_path, document_file.name)
            with open(file_path, mode='w+') as f:
                for chunk in document_file.chunks():
                    f.write(chunk)

            book.file_name = document_file.name
            book.save()
            url = '/book-list/?category-id=' + str(book.category_id)
            return HttpResponseRedirect(url)
    context_data['error'] = error
    context_data['msg'] = msg
    return render(request, 'new_book.html', context_data)


@login_required(login_url='/login/')
def book_list(request):
    context_data = dict()
    category_id = request.GET.get('category_id', None)
    if category_id:
        context_data['book_list'] = Book.objects.filter(category_id=category_id)
    else:
        context_data['book_list'] = Book.objects.order_by('-id')
    return render(request, 'book_list.html', context_data)


@login_required(login_url='/login/')
def download_book(request):
    book_id = request.GET.get('book_id', None)
    if book_id:
        book = Book.objects.filter(id=book_id).first()
        path = os.path.join(settings.BASE_DIR, 'uploads', str(book.id), book.file_name)
        data = ''
        with open(path) as f:
            data = f.read()
        response = HttpResponse(data, content_type="application/force-download")
        response['Content-Disposition'] = 'attachment; filename=' + os.path.basename(path)
        return response
    return HttpResponse('File Not Found')